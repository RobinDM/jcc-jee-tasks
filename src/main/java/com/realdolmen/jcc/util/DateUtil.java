package com.realdolmen.jcc.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static final DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    private DateUtil(){}

    public static Date parse(String dateString) {
        try {
            return df.parse(dateString);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String format(Date date){
        return df.format(date);
    }
}
