package com.realdolmen.jcc.controllers;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TasksController {

    public String getView() {
        return "WEB-INF/views/tasks.jsp";
    }
}
