package com.realdolmen.jcc.web;

import com.realdolmen.jcc.model.Task;

import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

@SessionScoped
public class TaskSession implements Serializable {

    private Task currentTask;

    public Task getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }
}
