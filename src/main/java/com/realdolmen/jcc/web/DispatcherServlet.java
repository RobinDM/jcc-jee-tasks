package com.realdolmen.jcc.web;

import com.realdolmen.jcc.controllers.TasksController;
import com.realdolmen.jcc.model.Task;
import com.realdolmen.jcc.model.TaskRepository;
import com.realdolmen.jcc.util.DateUtil;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet(urlPatterns = "/")
public class DispatcherServlet extends HttpServlet {

    @Inject
    private TaskSession taskSession;

    @Inject
    private TasksController tasksController;

    @Inject
    private TaskRepository taskRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
// Getting servlet request URL
        String url = request.getRequestURL()
                            .toString();

        // Getting servlet request query string.
        String queryString = request.getQueryString();

        // Getting request information without the hostname.
        String uri = request.getRequestURI();

        // Below we extract information about the request object path
        // information.
        String scheme = request.getScheme();
        String serverName = request.getServerName();
        int portNumber = request.getServerPort();
        String contextPath = request.getContextPath();
        String servletPath = request.getServletPath();
        String pathInfo = request.getPathInfo();
        String query = request.getQueryString();
        System.out.println("Url: " + url);
        System.out.println("Uri: " + uri);
        System.out.println("Scheme: " + scheme);
        System.out.println("Server Name: " + serverName);
        System.out.println("Port: " + portNumber);
        System.out.println("Context Path: " + contextPath);
        System.out.println("Servlet Path: " + servletPath);
        System.out.println("Path Info: " + pathInfo);
        System.out.println("Query: " + query);

        // === routing

        // trim off the leading slash
        String stripped = servletPath.substring(1)
                                     .trim();
        String view = null;
        switch (stripped) {
            case "":
                String task = request.getParameter("task");
                if (task != null) {
                    taskSession.setCurrentTask(taskRepository.findById(Long.parseLong(task)));
                } else {
                    taskSession.setCurrentTask(null);
                }
                request.setAttribute("tasks", taskRepository.getAllTasks());
                request.setAttribute("currentTask", taskSession.getCurrentTask());
                view = tasksController.getView();
                break;
        }
        if (view != null) {
            request.getRequestDispatcher(view)
                   .forward(request, response);
        } else {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.print("Url: " + url + "<br/>");
            pw.print("Uri: " + uri + "<br/>");
            pw.print("Scheme: " + scheme + "<br/>");
            pw.print("Server Name: " + serverName + "<br/>");
            pw.print("Port: " + portNumber + "<br/>");
            pw.print("Context Path: " + contextPath + "<br/>");
            pw.print("Servlet Path: " + servletPath + "<br/>");
            pw.print("Path Info: " + pathInfo + "<br/>");
            pw.print("Query: " + query);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String stripped = req.getServletPath()
                             .substring(1)
                             .trim();
        switch (stripped) {
            case "":
                // new task submitted
                Task newTask = new Task();
                newTask.setTitle(req.getParameter("title"));
                taskRepository.saveTask(newTask);
                resp.sendRedirect(req.getContextPath());
                break;
            case "update":
                String task1 = req.getParameter("task");
                long taskId = Long.parseLong(task1);
                System.out.println("Received request to update task with id " + taskId);
                String title = req.getParameter("editTitle");
                System.out.println("new title = " + title);
                String description = req.getParameter("editDescription");
                System.out.println("new description = " + description);
                Date deadline = DateUtil.parse(req.getParameter("editDeadline"));
                System.out.println("new deadline = " + DateUtil.format(deadline));
                Task toUpdate = taskRepository.findById(taskId);
                toUpdate.setTitle(title);
                toUpdate.setDescription(description);
                toUpdate.setDeadline(deadline);
                System.out.println("Updated task!");
                resp.sendRedirect(req.getContextPath());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String stripped = req.getServletPath()
                             .substring(1)
                             .trim();
        switch (stripped) {
            case "delete":
                String task1 = req.getParameter("task");
                long taskId = Long.parseLong(task1);
                Task deleteTask = taskRepository.findById(taskId);
                taskRepository.deleteTask(deleteTask);
                resp.sendRedirect(req.getContextPath());
        }
    }
}
