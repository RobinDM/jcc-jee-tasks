package com.realdolmen.jcc.model;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@RequestScoped
@Transactional
public class TaskRepository {

    @PersistenceContext
    EntityManager em;

    public List<Task> getAllTasks() {
        return em.createQuery("select t from Task t", Task.class)
                 .getResultList();
    }

    public void saveTask(Task task) {
        em.persist(task);
    }

    public void deleteTask(Task task) {
        em.remove(task);
    }

    public Task findById(Long id){
        return em.find(Task.class, id);
    }
}
