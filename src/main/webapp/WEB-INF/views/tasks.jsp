<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tasks</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <h3>Create a new task.</h3>
            <form method="post" class="form-inline">
                <div class="form-group">
                    <label for="newTaskName">Title</label>
                    <input id="newTaskName" type="text" class="form-control" name="title"/>
                </div>
                <input type="submit" class="btn btn-default" value="Add"/>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <h3>Task list.</h3>
            <ul>
                <c:forEach items="${tasks}" var="t">
                    <li>
                        <a href="<%= request.getContextPath() %>?task=${t.id}">${t.title}</a>
                    </li>
                </c:forEach>
            </ul>
        </div>
        <div class="col-xs-8">
            <h3>Task details.</h3>
            <c:choose>
                <c:when test="${null == currentTask}">
                    <div>
                        Select a task to edit/delete it.
                    </div>
                </c:when>
                <c:otherwise>
                    <form>
                        <div class="form-group">
                            <label for="currentTaskTitle">Title</label>
                            <input id="currentTaskTitle" type="text" name="editTitle" value="${currentTask.title}">
                        </div>
                        <div class="form-group">
                            <label for="currentTaskDescription">Description</label>
                            <input id="currentTaskDescription" type="text" name="editDescription" value="${currentTask.description}"/>
                        </div>
                        <div class="form-group">
                            <label for="currentTaskDeadline">Deadline</label>
                            <input id="currentTaskDeadline" type="text" name="editDeadline" value="${currentTask.deadline}"/>
                        </div>
                        <button type="submit" formmethod="post" formaction="<%= request.getContextPath() %>/update?task=${currentTask.id}">Update</button>
                        <button type="submit" formaction="<%= request.getContextPath() %>/delete?task=${currentTask.id}">Delete</button>
                    </form>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
</body>
</html>
