package com.realdolmen.jcc.model;

import com.realdolmen.jcc.AbstractPersistenceTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TaskRepositoryTest extends AbstractPersistenceTest {

    private TaskRepository taskRepository;

    @Before
    public void init() {
        taskRepository = new TaskRepository();
        taskRepository.em = em;
    }

    @Test
    public void getAllTasks() {
        List<Task> allTasks = taskRepository.getAllTasks();
        Assert.assertEquals(4, allTasks.size());
    }

    @Test
    public void saveTask() {
        Task newTask = new Task();
        newTask.setTitle("Dummy task!");
        taskRepository.saveTask(newTask);
        Assert.assertNotNull(newTask.getId());
    }
}